[//]: # (Topic: Transmute Projects Github)
[//]: # (Author: Tyrone Marshall)
[//]: # (Date: 2016.06.08)
[//]: # (Format: markdown)
[//]: # (Version 2016)

![logo](https://bitbucket.org/pwenergylab/transmute_projects/raw/master/images/transmute.png)
#Transmute Projects
An Energy Lab Research Private Github Repository
***
The effort to cause a change in form of raw GH xml definition via an environment for collaboration, automation and conversion for projects.